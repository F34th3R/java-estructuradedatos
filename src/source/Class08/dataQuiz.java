/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class08;

import java.util.ArrayList;
import java.util.Iterator;

public class dataQuiz {

    private ArrayList<Integer> optionsNumber = new ArrayList<>();
    private Iterator<Integer> myIntegerIterator = this.optionsNumber.iterator();
    private SetGet setGet = new SetGet();

    public void addNumber(Integer value) {
        this.optionsNumber.add(value);
    }

    public void sumNumber() {
        int sum = 0;
        while (myIntegerIterator.hasNext()) {
            sum += myIntegerIterator.next();
        }
        setGet.setSuma(sum);
    }

    public void averageNumber() {
        int sum = 0;
        int count = 0;
        while (myIntegerIterator.hasNext()) {
            count = count + 1;
            sum += myIntegerIterator.next();
        }
        setGet.setAverage((sum / count));
    }

    public void minNumber() {
        int min = 0;

        while (myIntegerIterator.hasNext()) {
            if (myIntegerIterator.next() < min) {
                min = myIntegerIterator.next();
            }
        }
        setGet.setMin(min);
    }

    public void maxNumber() {
        int max = 0;

        while (myIntegerIterator.hasNext()) {
            if (myIntegerIterator.next() > max) {
                max = myIntegerIterator.next();
            }
        }
        setGet.setMax(max);
    }

    public void displayList() {

    }

}
