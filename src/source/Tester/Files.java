/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Tester;

import java.io.*;

public class Files {
    public static void createFile(String fileName) {
        File file = new File(fileName);
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter(file));
            printWriter.close();
            System.out.printf("The file was create!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeFile(String fileName, String myContent) {
        File file = new File(fileName);
        try {
            PrintWriter exit = new PrintWriter(new FileWriter(file));
            exit.println(myContent);
//            exit.println("--------------------------------------------");
            exit.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readFile(String fileName) {
        File file = new File(fileName);
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String reader;
            reader = bufferedReader.readLine();
            while (reader != null) {
                System.out.println("Reader: " + reader);
                reader = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void appendFile(String fileName, String newContent) {
        File file = new File(fileName);
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter(fileName, true));
            printWriter.println(newContent);
            printWriter.println("--------------------------------------------");
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
