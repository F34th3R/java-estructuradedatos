package source;

import source.Class01.*;
import source.Class02.*;
import source.Class03.*;

public class Main {
    public static void main(String[] args) {

        Class01_Main class01 = new Class01_Main();
        Class02_Main class02 = new Class02_Main();
        Class03_Main class03 = new Class03_Main();

        Menu menu = new Menu("Menu", true);
        menu.addOption(1, "Class 01 // DONE");
        menu.addOption(2, "Class 02");
        menu.addOption(3, "Class 03");
        menu.addOption(4, "Class 04");
        menu.addOption(5, "Class 05");
        menu.optionExit();

        while (menu.getMenuEnable()) {
            menu.displayMenu();
            // Option 01 / Class 01
            if (menu.getScannerValue() == 1) {
                class01.menu();
            }
            else if (menu.getScannerValue() == 2) {
                class02.menu();
            }
            else if (menu.getScannerValue() == 3) {
                class03.menu();
            }
            else if (menu.getScannerValue() == 4) {
                class03.menu();
            }
            else if (menu.getScannerValue() == 5) {
                class03.menu();
            }
            else if (menu.getScannerValue() == 0) {
                menu.endTask();
            }

        }
    }
}
