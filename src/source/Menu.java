
/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Menu {

    static final String ANSI_RESET = "\u001B[0m";
    static final String ANSI_BLACK = "\u001B[30m";
    static final String ANSI_RED = "\u001B[31m";
    static final String ANSI_GREEN = "\u001B[32m";
    static final String ANSI_YELLOW = "\u001B[33m";
    static final String ANSI_BLUE = "\u001B[34m";
    static final String ANSI_PURPLE = "\u001B[35m";
    static final String ANSI_CYAN = "\u001B[36m";
    static final String ANSI_WHITE = "\u001B[37m";

    static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";


    ArrayList<String> menuOption = new ArrayList<>();
    ArrayList<Integer> numOption = new ArrayList<>();

    Scanner myScanner = new Scanner(System.in);

    String menuTitle;
    int scannerValue;
    Boolean menuEnable;

    // Getters and Setters
    public Boolean getMenuEnable() {
        return menuEnable;
    }
    public void setMenuEnable(Boolean menuEnable) { this.menuEnable = menuEnable; }

    public int getScannerValue() { return scannerValue; }
    public void setScannerValue(int scannerValue) { this.scannerValue = scannerValue; }

    // Constructors
    public Menu(String menuTitle, Boolean menuEnable) {
        this.menuTitle = menuTitle;
        this.menuEnable = menuEnable;
    }

    // Methods
    // Close the menu
    public void endTask() {
        if (getScannerValue() == 0 ) {
            setMenuEnable(false);
        }
    }

    // Add default exit option
    public void optionExit() {
        menuOption.add("Exit");
        numOption.add(0);
    }

    // Add default exit option
    public void optionBack() {
        menuOption.add("Back");
        numOption.add(0);
    }

    // Add option to the menu
    public void addOption(int num, String option) {
        this.menuOption.add(option);
        this.numOption.add(num);
    }

    // Display the options
    public void displayMenu() {
        System.out.printf(ANSI_CYAN +"%s%n" + ANSI_RESET, menuTitle);
        if (this.menuOption.size() != 0 ) {
            Iterator<String> myItOption = menuOption.iterator();
            Iterator<Integer> myItNumOption = numOption.iterator();

            while (myItOption.hasNext()) {
                System.out.printf(ANSI_RED + "[%o] " + ANSI_RESET + "%s%n", myItNumOption.next(), myItOption.next());
            }
            captureValue();
        }
        else {
            System.out.println("Is empty");
        }
    }

    // Capture the Scanner Value
    private void captureValue() {
        System.out.print(ANSI_YELLOW + "\nMy option:" + " ");
        setScannerValue(Integer.parseInt(myScanner.nextLine()));
        System.out.print("----------------------------------\n" + ANSI_RESET);
    }

    public void personaliteMessage(String message, String color) {
        switch (color) {
            case "red":
                System.out.printf(ANSI_RED + "%s%n" + ANSI_RESET, message);
                break;
            case "blue":
                System.out.printf(ANSI_BLUE + "%s%n" + ANSI_RESET, message);
                break;
            case "cyan":
                System.out.printf(ANSI_CYAN + "%s%n" + ANSI_RESET, message);
                break;
            case "yellow":
                System.out.printf(ANSI_YELLOW + "%s%n" + ANSI_RESET, message);
                break;
            case "green":
                System.out.printf(ANSI_GREEN + "%s%n" + ANSI_RESET, message);
                break;
            case "purple":
                System.out.printf(ANSI_PURPLE + "%s%n" + ANSI_RESET, message);
                break;
            case "black":
                System.out.printf(ANSI_BLACK + "%s%n" + ANSI_RESET, message);
                break;
            default:
                System.out.printf(ANSI_WHITE + "%s%n" + ANSI_RESET, message);
                break;
        }
        System.out.printf(ANSI_YELLOW + "%s%n" + ANSI_RESET, message);
    }
}