package source.Class06;

import source.Class06.Classwork.ArbolBinario;

import javax.swing.*;

public class Classs05_Main {

    public static void main(String args[]) {
        int opciones = 0;
        int elementos;
        String nombre;
        ArbolBinario arbolBinario = new ArbolBinario();
        do {
            try {
                opciones = Integer.parseInt(JOptionPane.showInputDialog(null,
                        "1. Agregar un nuevo nodo al arbol de la madre\n" +
                                "2 Salir\n" +
                                "Elija una opcion...", "Arbol binario", JOptionPane.QUESTION_MESSAGE));
                switch (opciones) {
                    case 1:
                        elementos = Integer.parseInt(JOptionPane.showInputDialog(null,
                                "Ingresar el numero del padre", "Agregar nodo",
                                JOptionPane.QUESTION_MESSAGE));
                        nombre = JOptionPane.showInputDialog(null,
                                "Ingrese el numero del nodo...",
                                "Fin", JOptionPane.QUESTION_MESSAGE);
                        break;
                    case 2:
                        JOptionPane.showInputDialog(null,
                                "Aplicacion finalizada",
                                "Fin",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                    default:
                        JOptionPane.showInputDialog(null,
                                "Opcion incorrecta de la madre",
                                "error",
                                JOptionPane.INFORMATION_MESSAGE);
                        break;
                }
            }
            catch (NumberFormatException e) {
                JOptionPane.showInputDialog(null,
                        "Error de la madre",
                        e.getMessage());
            }
        }
        while(opciones != 2);
    }
}
