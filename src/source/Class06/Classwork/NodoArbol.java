/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class06.Classwork;

public class NodoArbol {

    int dato;
    String nombre;
    NodoArbol hijoIzquierda;
    NodoArbol hijoDerecha;

    public NodoArbol(int dato, String nombre) {
        this.dato = dato;
        this.nombre = nombre;
        this.hijoIzquierda = null;
        this.hijoDerecha = null;
    }
}
