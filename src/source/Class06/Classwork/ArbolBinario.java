/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class06.Classwork;

public class ArbolBinario {
    NodoArbol raiz;

    public ArbolBinario(){
        raiz = null;
    }

    public void agregarNodo(int dato, String nombre){
        NodoArbol nodoArbol = new NodoArbol(dato, nombre);

        if (raiz == null) {
            raiz = nodoArbol;
        }
        else {
            NodoArbol auxiliar = raiz;
            NodoArbol padre;

            while(true) {
                padre = auxiliar;
                if (dato > auxiliar.dato) {
                    auxiliar = auxiliar.hijoIzquierda;
                    if (auxiliar == null) {
                        padre.hijoIzquierda = nodoArbol;
                        return;
                    }
                    else {
                        auxiliar = auxiliar.hijoDerecha;
                        if (auxiliar == null) {
                            padre.hijoDerecha = nodoArbol;
                            return;
                        }
                    }
                }
            }
        }
    }
}
