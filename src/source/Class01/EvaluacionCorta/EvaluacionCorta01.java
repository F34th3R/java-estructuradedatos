/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class01.EvaluacionCorta;

import source.Menu;

import java.util.Scanner;

public class EvaluacionCorta01 {
    public void menu() {
        Menu menu = new Menu("Evaluacion Corta", true);
        menu.optionBack();

        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 999) {
                // a)
                int myArray[][] = new int[2][1];

                // b)
                // R = filas = 3

                // c)
                // R = columanas = 2

                // d)
                // R = 6

                // e)
                System.out.println("fila 1");
                for (int i = 0; i < 2; i++) {
                    System.out.println(myArray[0][i]);
                }
                // f)
                System.out.println("columna 2");
                for (int i = 0; i < 1; i++) {
                    System.out.println(myArray[i][1]);
                }
                // g)
                myArray[0][1] = 0;
                System.out.println(myArray[0][1]);
                // h) y i)
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 1; j++) {
                        myArray[i][j] = 0;
                    }
                }
                // j)
                Scanner scanner = new Scanner(System.in);
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 1; j++) {
                        System.out.println("Valor a ingresar = ");
                        int temp = scanner.nextInt();
                        myArray[i][j] = temp;
                    }
                }
                // k)
                mayorMenor(myArray);
                // l)
                for (int i = 0; i < 2; i++) {
                    System.out.printf("%o", myArray[i][0]);
                }
                // m)
                for (int i = 0; i < 2; i++) {
                    int temp = myArray[i][2];
                    temp = temp + temp;
                    System.out.printf("%o", temp);
                }
            }
            else {
                menu.personaliteMessage("The value option is incorrect", "yellow");
            }
            menu.endTask();
        }
    }
    public void mayorMenor(int[][] value ) {
        int mayor=0;
        int menor=999999999;
        int i,j;
        int temp=0;
        int aux=0;
        i=0;
        while (i < value.length){
            for ( j=0;j<value[i].length;j++){
                if (value[i][j] > value[i][value.length-1]){
                    temp = value[i][j];
                }
                if (value[i][j] >  value[i][value[i].length-1] ){
                    aux = value [i][value[i].length-1];
                }
            }
            if (temp > mayor){
                mayor = temp;
            }
            if (aux < menor){
                menor = aux;
            }
            i++;
        }
        System.out.println("Elro mayor de la matriz es el: "+ mayor);
        System.out.println("Elro menor de la matriz es el: "+menor);
    }
}
