/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class01.GuiaDeEjercicios;

import java.util.Scanner;

public class GuiaEjercicio01 {
    private Scanner scanner = new Scanner(System.in);

    public void part_01() {
        int workers = 10;
        float arrayA[] = new float[workers + 1];
        int arrayB[] = new int[workers + 1];
        int arrayC[] = new int[11];

        System.out.println("Este programa recibe los rangos en los que estan");
        System.out.printf("\nlos salarios de los %d empleados.\n", workers);

        float ventas;

        System.out.println("Utilicele un arreglo unidimensional para resolver el siguiente problema: \n" +
                "una compañía paga a sus vendedores por comisión. Los vendedores reciben $200 por semana  más el 9% de \n" +
                "sus ventas totales de esa semana. Por ejemplo, un vendedor que acumule $5000 en ventas en una semana recibirá \n" +
                "$200 más el 9% de $5000, o un total de $650.00 Escriba una aplicación (utilizando un arreglo de contadores) que \n" +
                "determine cuántos vendedores recibieron salarios en cada uno de los siguientes rangos. (Suponga que el salario de \n" +
                "cada vendedor su trunca a una cantidad entera):\n");

        for (int j = 0; j < workers; j++ ) {
            arrayC[j] = 0;
        }
        for (int i = 1; i <= workers; i++ ) {
            System.out.printf("\n\nIntroduzca las ventas del empleado num %d  ", i);
            ventas = scanner.nextFloat();
            arrayA[i] = ((float)(9)/100)*ventas + 200;
            System.out.printf("\nEl salario del empleado %d es: %.2f\n ", i, arrayA[i]);
            arrayB[i] = (int)(arrayA[i])/100;
        }

        for (int k = 1; k <= workers; k++ ) {
            if (arrayB[k] < 10)
                arrayC[arrayB[k]]++;
            else
                arrayC[workers]++;
        }
        for (int j = 2; j < workers; j++ ) {
            System.out.printf("\nHay %d empleados que cobran entre %d", arrayC[j], (j * 100));
            System.out.printf(" y %d pesos.\n", (( j + 1 ) * 100 ) - 1 );
        }
        System.out.printf("Hay %d empleados que cobran 1000 o mas.\n", arrayC[workers]);
    }

    public void part_02() {
        int numbers = 10;
        int arrayA[] = new int[numbers];

        int inputValue;
        String tempString;
        int tempInt = 0;
        System.out.println("escriba instrucciones que realizan las siguientes operaciones con arreglos unidimensionales:/n" +
                "a) Asignar cero a los 10 elementos del arreglo (cuentas de tipo entero)\n" +
                "b) Sumar uno a cada uno de los elementos del arreglo (bono de tipo entero)\n" +
                "c) Imprimir el valor mayor");
        System.out.println("\nAgregar numeros al Array:");
        for (int i = 0; i < numbers; i++) {
            arrayA[i] = 0;
        }
        for (int i = 0; i <= numbers; i++) {
            if (i == 0) {
                System.out.printf("Introduzca el num del array\n");
            }
            else {
                System.out.printf("Introduzca el siguiente num del array\n");
            }
            inputValue = scanner.nextInt();
            tempString = Integer.toString(inputValue) + "0";
            arrayA[i] = Integer.parseInt(tempString);
            System.out.println("El num se le puso un 0 al final = " + arrayA[i]);
            arrayA[i] = arrayA[i] + 1;
            System.out.println("El num se le sumo un 1 = " + arrayA[i]);
        }
        for(int i=0; i < arrayA.length; i++){
            if(arrayA[i] > tempInt){ //
                tempInt = arrayA[i];
            }
        }
        System.out.println("El num mayor del array es = " + Integer.toString(tempInt));
    }

    ////////////////////
    enum state {SI, NO};
    private int size = 5;
    private int num;
    private int inputNumber = 0;
    state printValue;
    int myArray[] = new int[size];

    public void part_03() {

        for (int n = 0; n < size; n++ ) {
            num = 0;
            while ( (10 > num) || ( 100 < num) ) {
                System.out.println("\nPor favor introduzca un num entre 10 y 100 inclusive\n");
                num = scanner.nextInt();
            }
            myArray[n] = num;
            inputNumber++;
            display();
        }
        System.out.println("\n");
    }
    private void display()
    {
        System.out.printf("\nLos numeros recibidos: %d", inputNumber);
        printValue = state.SI;

        System.out.printf("\n%d", myArray[0]);

        for (int j = 1; j < inputNumber; j++ )
        {  // Abre for
            printValue = state.SI;

            for ( int k = 0; k < j; k++)
            {
                if ( myArray[j] == myArray[k] )
                    printValue = state.NO;
            }

            if ( state.SI == printValue)
            {
                System.out.printf("\n%d", myArray[j]);
            }
        }
    }
}
