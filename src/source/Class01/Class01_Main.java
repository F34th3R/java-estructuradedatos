/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class01;

import source.Menu;
import source.Class01.EvaluacionCorta.*;
import source.Class01.TrabajoEnClases.*;

public class Class01_Main {
    public void menu() {
        EvaluacionCorta01 homework_main = new EvaluacionCorta01();
        class01_classwork_Main classwork_main = new class01_classwork_Main();

        Menu menu = new Menu("Clase 01", true);
        menu.addOption(1, "Guía de Ejercicios");
        menu.addOption(2, "Evaluación Corta 1");
        menu.optionBack();

        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                classwork_main.menu();
            }
            else if (menu.getScannerValue() == 2) {
                homework_main.menu();
            }
            else {
                menu.personaliteMessage("The value option is incorrect", "yellow");
            }
            menu.endTask();
        }
    }
}
