/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class01.TrabajoEnClases;

import source.Class01.GuiaDeEjercicios.GuiaEjercicio01;
import source.Menu;

public class class01_classwork_Main {
    public void menu() {
        GuiaEjercicio01 guiaEjercicio01 = new GuiaEjercicio01();

        Menu menu = new Menu("Guia de Ejercicios", true);
        menu.addOption(1, "Ejercicio #1");
        menu.addOption(2, "Ejercicio #2");
        menu.addOption(3, "Ejercicio #3");
        menu.optionBack();

        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                guiaEjercicio01.part_01();
            }
            else if (menu.getScannerValue() == 2) {
                guiaEjercicio01.part_02();
            }
            else if (menu.getScannerValue() == 3) {
                guiaEjercicio01.part_03();
            }
            else {
                menu.personaliteMessage("The value option is incorrect", "yellow");
            }
            menu.endTask();
        }
    }
}
