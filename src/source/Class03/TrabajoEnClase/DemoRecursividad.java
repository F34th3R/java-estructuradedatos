/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class03.TrabajoEnClase;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DemoRecursividad {
    public void downStairs(int stairs) {
        if (stairs == 0) {
            System.out.printf("%s%n", "Has bajado completamente la escalera");
        }
        else {
            try {
                Thread.sleep(500);
                System.out.printf("%s%n", "Bajando escaleras" + stairs);
                downStairs(stairs - 1);
            }
            catch (InterruptedException e) {
                Logger.getLogger(DemoRecursividad.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
}
