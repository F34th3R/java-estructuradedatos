/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class03.TrabajoEnClase;

import source.Menu;

import java.util.Scanner;

public class class03_classwork_Main {
    public void menu() {
        DemoRecursividad demo = new DemoRecursividad();
        factorialRecursivo factorial = new factorialRecursivo();
        Scanner myScanner = new Scanner(System.in);

        Menu menu = new Menu("Recursividad", true);
        menu.addOption(1, "Demo");
        menu.addOption(2, "Factorial");
        menu.optionBack();

        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                demo.downStairs(20);
            }
            else if (menu.getScannerValue() == 2) {
                // TODO ***
//                factorial.factorial(menu.scValueInteger());
            }
            else {
                menu.personaliteMessage("The value option is incorrect", "yellow");
            }
            menu.endTask();
        }
    }
}
