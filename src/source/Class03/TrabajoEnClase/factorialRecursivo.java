/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class03.TrabajoEnClase;

public class factorialRecursivo {
    public int factorial(int n) {
        if (n < 0) {
            return 0;
        }
        else {
            if (n == 0) {
                return 1;
            }
            else {
                return n * factorial(n-1);
            }
        }
    }
}
