/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class03;

import source.Class03.TrabajoEnClase.*;
import source.Menu;

public class Class03_Main {
    public void menu() {
        class03_classwork_Main classwork_main = new class03_classwork_Main();

        Menu menu = new Menu("Menu: Class 03", true);
        menu.addOption(1, "ClassWork");
        menu.addOption(2, "HomeWork");
        menu.optionBack();

        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                classwork_main.menu();
            }
            else if (menu.getScannerValue() == 2) {
//                homework_main.menu();
            }
            else {
                menu.personaliteMessage("The value option is incorrect", "yellow");
            }
            menu.endTask();
        }
    }
}
