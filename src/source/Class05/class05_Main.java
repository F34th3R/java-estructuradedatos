/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class05;

import source.Class05.HomeWork.HomeWork05;
import source.Class05.classwork.Files;
import source.Class05.GuiaEnClases.guiaDeEjercicios;
import source.Menu;

import java.util.Scanner;

public class class05_Main {
    public void menu() {
        guiaDeEjercicios guiaDeEjercicios = new guiaDeEjercicios();
        HomeWork05 homeWork05 = new HomeWork05();

        Menu menu = new Menu("Clase 05", true);
        menu.addOption(1, "Trabajo en clase");
        menu.addOption(2, "Guía de Ejercicios");
        menu.addOption(3, "Tarea");
        menu.optionBack();

        Scanner scanner = new Scanner(System.in);
        String PATH = "C:\\TesterJava\\EstructuraDeDatos.txt";
        Files.createFile(PATH);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                Files.writeFile(PATH, "Test");
                Files.appendFile(PATH, "Nuevo");
                Files.readFile(PATH);
            }
            else if (menu.getScannerValue() == 2) {
                guiaDeEjercicios.guiaEjercicios05();
            }
            else if(menu.getScannerValue() == 3) {
                homeWork05.homeWork05();
            }
            menu.endTask();
        }
    }
}
