/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class05.HomeWork;

import source.Menu;

public class HomeWork05 {
    public void homeWork05() {
        Menu menu = new Menu("Tarea 05", true);
        menu.addOption(1, "");
        menu.optionBack();
        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                System.out.println();
            }
            else if (menu.getScannerValue() == 2) {
                System.out.println();
            }
            menu.endTask();
        }
    }
}
