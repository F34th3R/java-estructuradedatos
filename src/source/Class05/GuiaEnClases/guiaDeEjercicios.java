/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class05.GuiaEnClases;

import source.Class05.classwork.Files;
import source.Menu;

import java.util.Scanner;

public class guiaDeEjercicios {
    public void guiaEjercicios05() {
        String FILE_NAME = "C:\\TesterJava\\log.txt";
        Files.createFile(FILE_NAME);
        Scanner scanner = new Scanner(System.in);

        Menu menu = new Menu("\nAgenda Suprema", true);
        menu.addOption(1, "Agregar nuevo Contacto");
        menu.addOption(2, "Desplegar Contacto");
        menu.optionExit();

        String temp;
        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                System.out.println("Ingrese el nombre del contacto");
                temp = scanner.nextLine();
                Files.appendFile(FILE_NAME, temp);
                System.out.println("Ingrese el numero del contacto");
                temp = scanner.nextLine();
                Files.appendFile(FILE_NAME, temp);
                System.out.println("Se agrego el nuevo contacto");
            }
            else if (menu.getScannerValue() == 2) {
                Files.readFile(FILE_NAME);
            }
            menu.endTask();
        }
    }
}
