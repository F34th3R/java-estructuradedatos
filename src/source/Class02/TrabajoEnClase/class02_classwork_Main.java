/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class02.TrabajoEnClase;

import source.Menu;

import java.util.Scanner;

public class class02_classwork_Main {
    public void menu() {
        List list = new List();
        Scanner scanner = new Scanner(System.in);

        Menu menu = new Menu("ClassWork: LinkedList", true);
        menu.addOption(1, "Insert Book at the beginning");
        menu.addOption(2, "Insert Book at the end");
        menu.addOption(3, "Insert Book after");
        menu.addOption(4, "Delete Book at the beginning");
        menu.addOption(5, "Delete Book at the end");
        menu.addOption(6, "Delete (Position)");
        menu.addOption(7, "Get length");
        menu.optionBack();


        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                list.insertFirst(newBook());
            }
            else if (menu.getScannerValue() == 2) {
                list.insertEnd(newBook());
            }
            else if (menu.getScannerValue() == 3) {
                menu.personaliteMessage("Insert after (book number)", "red");
                int value = scanner.nextInt();
                list.insertAfter(value, newBook());
            }
            else if (menu.getScannerValue() == 4) {
                list.deleteFirst();
            }
            else if (menu.getScannerValue() == 5) {
                list.deleteEnd();
            }
            else if (menu.getScannerValue() == 6) {
                menu.personaliteMessage("Delete position book (book number)", "red");
                int value = scanner.nextInt();
                list.deleteBook(value);
            }
            else if (menu.getScannerValue() == 7) {
                list.count();
            }
            menu.endTask();
        }
    }

    private Book newBook() {
        Scanner scanner = new Scanner(System.in);
        Book book = new Book();
        System.out.println("Name the title of the new book: ");
        book.setTittle(scanner.nextLine());
        System.out.println("Name the Author of the new book: ");
        book.setAuthor(scanner.nextLine());
        System.out.println("Name the ISBN of the new book: ");
        book.setIsbn(scanner.nextLine());
        return book;
    }
}
