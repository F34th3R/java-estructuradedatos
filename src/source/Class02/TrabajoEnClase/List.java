/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class02.TrabajoEnClase;

public class List {

    private Node head = null;
    private int length = 0;

    private class Node {
        public Book book;
        public Node next = null;

        public Node(Book book) {
            this.book = book;
        }
    }

    public void insertFirst(Book book) {
        Node node = new Node(book);
        node.next = head;
        head = node;
        length++;
    }

    public void insertEnd(Book book) {
        Node node = new Node(book);
        if (head == null) {
            head = node;
        }
        else {
            Node pointer = head;
            while (pointer.next != null) {
                pointer = pointer.next;
            }
            pointer.next = node;
        }
        length++;
    }

    public void insertAfter(int n, Book book) {
        Node node = new Node(book);
        if (head == null) {
            head = node;
        }
        else {
            Node pointer = head;
            int counter = 0;
            while (counter < n && pointer.next != null) {
                pointer = pointer.next;
                counter++;
            }
            node.next = pointer.next;
            pointer.next = node;
        }
        length++;
    }

    public Book get(int n) {
        if (head == null) {
            return null;
        }
        else {
            Node pointer = head;
            int counter = 0;
            while (counter < n && pointer.next != null) {
                pointer = pointer.next;
                counter++;
            }
            if (counter != n) {
                return null;
            }
            else {
                return pointer.book;
            }
        }
    }

    public int count() {
        return length;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public void deleteFirst() {
        if (head == null) {
            Node first = head;
            head = head.next;
            first.next = null;
            length--;
        }
    }

    public void deleteEnd() {
        if (head == null) {
            if (head.next == null) {
                head = null;
                length--;
            }
            else {
                Node pointer = head;
                while (pointer.next.next == null) {
                    pointer = pointer.next;
                }
                pointer.next = null;
                length--;
            }
        }
    }

    public void deleteBook(int n) {
        if (head != null) {
            if (n == 0) {
                Node first = head;
                head = head.next;
                first.next = null;
                length--;
            }
            else if (n < length){
                Node pointer = head;
                int counter = 0;
                while (counter < (n - 1)) {
                    pointer = pointer.next;
                    counter++;
                }
                Node temp = pointer.next;
                pointer.next = temp.next;
                temp.next = null;
                length--;
            }

        }
    }
}
