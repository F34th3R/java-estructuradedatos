/*
 * Copyright (c) 2018. This code is owned by Jafet L.
 * Please use carefully...
 */

package source.Class02;

import source.Class02.TrabajoEnClase.*;
import source.Class02.EnCasa.*;
import source.Menu;

public class Class02_Main {
    public void menu() {
        class02_classwork_Main classwork_main = new class02_classwork_Main();
        class02_homework_Main homework_main = new class02_homework_Main();

        Menu menu = new Menu("Menu: Class 01 / LinkedList", true);
        menu.addOption(1, "ClassWork");
        menu.addOption(2, "HomeWork");
        menu.optionBack();

        menu.setMenuEnable(true);
        while (menu.getMenuEnable()) {
            menu.displayMenu();
            if (menu.getScannerValue() == 1) {
                classwork_main.menu();
            }
            if (menu.getScannerValue() == 2) {
                homework_main.menu();
            }
            menu.endTask();
        }
    }
}
